import datetime
import logging
import os
import pickle as pkl
from typing import Dict, Optional, Sequence, Union

import numpy as np
import pandas as pd
from PIL import Image
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn.preprocessing import StandardScaler

from itemized_billing.utility import definitions as defn
from itemized_billing.utility import doc_data, utility
from itemized_billing.ocr import ocr

desired_width = 780
pd.set_option('display.width', desired_width)
pd.set_option('display.max_columns', None)


class PageClassifierData(object):

    train_doc_dir: str
    model_store_dir: str
    formatted_date_str: str
    model_df: pd.DataFrame
    X: Optional[pd.Series]
    y: Optional[np.ndarray]
    predictions: Optional[np.ndarray]
    scaler: Optional[StandardScaler]
    model: Optional[object]

    def __init__(self, ddo: doc_data.DocumentData, source: str) -> None:

        self.logger = logging.getLogger(__name__)

        self.ddo = ddo
        self.source = source
        self.train_doc_dir = defn.BASE_DIR + "/" + defn.TRAINING_DOCUMENT_DIR  # training document directory
        self.model_store_dir = defn.BASE_DIR + "/" + defn.MODEL_STORE_DIR  # model storage directory
        self.formatted_date_str = datetime.datetime.now().strftime("%Y_%m_%d_%Hhr_%Mmin_%Ssec")

        fields_dct: Dict[str, Sequence[Union[float, str]]] = {'class': [],  # 1 is an ib page, 0 is not an ib page
                                                              'number_count': [],
                                                              'word_count': [],
                                                              'hybrid_count': [],
                                                              'spaces_count': [],
                                                              'nan_count': [],
                                                              'max_word_height': [],
                                                              'average_string_length': [],
                                                              'average_font_size': [],
                                                              'total_string_length': [],
                                                              'dollar_sign_count': [],
                                                              'word_fax_count': [],
                                                              'word_cover_count': [],
                                                              'word_sheet_count': [],
                                                              'word_inquiry_count': [],
                                                              'word_consideration_count': [],
                                                              'word_notary_count': []
                                                              }

        self.model_df = pd.DataFrame(fields_dct)  # derived data is stored here
        self.X = None
        self.y = None
        self.predictions = None
        self.scaler = None
        self.model = None

    @staticmethod
    def mask_count(mask, count_type: bool = True) -> int:
        """

        :param mask:
        :param count_type: {True/False} count the True values, else count the False values
        :return:
        """

        keys = mask.value_counts().keys().tolist()

        try:
            true_index = keys.index(count_type)
            count = mask.value_counts().tolist()[true_index]
        except ValueError:
            count = 0

        return count

    def get_latest_model(self):
        """ load the latest model/scaler to the object """

        for sv in ["model", "scaler"]:
            name = utility.get_files_by_type(filepath=defn.MODEL_STORE_DIR,
                                             filetype=sv,
                                             sort="DATE_DESC")[0]

            path = defn.MODEL_STORE_DIR + "/" + name

            try:
                with open(path, 'rb') as file:
                    if sv == "model":
                        self.model = pkl.load(file=file)
                    elif sv == "scaler":
                        self.scaler = pkl.load(file=file)
            except FileNotFoundError:
                _exit_str = "Unable to load {0} ({1}).".format(sv, name)
                self.logger.info(_exit_str)
                raise SystemExit(_exit_str)

    def calculate_derived_data(self) -> None:
        """ grab files, ocr, build derived data for files """

        # TODO add calculations for
        #    $ dollar sign count

        if self.source == defn.SOURCE_TRAIN_CONST:
            self.ddo.png_dir = self.train_doc_dir
            self.ddo.png_filenames = utility.get_files_by_type(filepath=self.ddo.png_dir,
                                                               filetype="png",
                                                               sort="REVERSE_FILENAME")
        elif self.source == defn.SOURCE_PREDICT_CONST:
            self.ddo.png_dir = self.ddo.png_dir
        else:
            _exit_str = "A source must be specified."
            self.logger.info(_exit_str)
            raise ValueError(_exit_str)

        for filename in self.ddo.png_filenames:
            load_image_from = self.ddo.png_dir + "/" + filename
            self.logger.debug(load_image_from)

            # grab image dimensions for scaling some variables
            im = Image.open(load_image_from)
            img_width, img_height = im.size

            ocr_result = ocr.OCRImage(load_image_from, ['gray', 'resize'])
            ocr_result.preprocess_image(resize_factor=1.45, view_transforms=False)
            ocr_result.apply_ocr(read_config='--psm 11')  # '--psm 6'

            # convert all ocr'd text to lowercase
            ocr_result.ocr_result_df['text'] = ocr_result.ocr_result_df['text'].str.lower()

            curr_idx = len(self.model_df)

            # load class values
            class_str = filename.split("-")[0]
            if class_str == "ncp":
                self.model_df.loc[curr_idx, 'class'] = int(1)
            elif class_str == "cp":
                self.model_df.loc[curr_idx, 'class'] = int(0)

            # find max text height
            conf_not_neg_1: pd.DataFrame = ocr_result.ocr_result_df[ocr_result.ocr_result_df['conf'] != -1]
            cnn1_grouped: pd.DataFrame = conf_not_neg_1.groupby(['block_num']).mean()['height']
            max_word_height: float = cnn1_grouped.max() / float(img_height) * 100.0
            self.model_df.loc[curr_idx, 'max_word_height'] = max_word_height

            # calculate average string length
            string_len_ls = ocr_result.ocr_result_df['text'].str.len()
            self.model_df.loc[curr_idx, 'average_string_length'] = np.nanmean(string_len_ls)

            # calculate word counts
            contained_string_ls = ['fax', 'cover', 'sheet', 'inquiry', 'consideration', 'notary']
            for string in contained_string_ls:
                contains_mask = ocr_result.ocr_result_df['text'].str.contains(string)
                string_count = self.mask_count(mask=contains_mask, count_type=True)
                self.model_df.loc[curr_idx, 'word_{0}_count'.format(string)] = string_count

            # calculate total string length
            self.model_df.loc[curr_idx, 'total_string_length'] = len(ocr_result.ocr_result_df)

            # calculate nan count
            nan_mask = ocr_result.ocr_result_df['text'].isnull()
            nan_count = self.mask_count(mask=nan_mask, count_type=True)
            self.model_df.loc[curr_idx, 'nan_count'] = nan_count

            # calculate number count (this will include dates i.e. 1/17/2019 will yield True)
            only_alphanumeric = ocr_result.ocr_result_df['text'].str.replace(r'\W+', '')
            number_mask = only_alphanumeric.str.contains("^\\d+$", na=False, regex=True)
            number_count = self.mask_count(mask=number_mask, count_type=True)
            self.model_df.loc[curr_idx, 'number_count'] = number_count

            # calculate word count
            word_mask = ocr_result.ocr_result_df['text'].str.contains("^[a-zA-Z]+$", na=False, regex=True)
            word_count = self.mask_count(mask=word_mask, count_type=True)
            self.model_df.loc[curr_idx, 'word_count'] = word_count

            # calculate hybrid count
            self.model_df.loc[curr_idx, 'hybrid_count'] = len(ocr_result.ocr_result_df) - word_count - number_count - nan_count

            # calculate average font size
            nan_mask = ocr_result.ocr_result_df['conf'] != -1
            self.model_df.loc[curr_idx, 'average_font_size'] = ocr_result.ocr_result_df[nan_mask]['height'].mean()

        # drop columns that have nan values in them
        self.model_df = self.model_df.dropna(axis=1)

    def get_X_and_y(self) -> None:
        # build a classifier using self.model_df
        self.X = self.model_df.loc[:, self.model_df.columns != 'class']

        # scale X
        if self.source == defn.SOURCE_TRAIN_CONST:
            self.scaler = StandardScaler()
            self.scaler.fit(self.X)
            # for predictions we split class into its own array
            self.y = self.model_df['class']

        self.X = self.scaler.transform(self.X)

    def train_model(self) -> None:

        self.get_X_and_y()

        clf = LogisticRegression(random_state=0,
                                 solver="lbfgs",
                                 max_iter=2000,
                                 multi_class="auto").fit(self.X, self.y)

        self.model: object = clf

    def save_model(self, keep: int = 3) -> None:

        # logic to save the model and the scaler
        for sv in ["model", "scaler"]:
            self.logger.debug(sv)

            save_loc: str = self.model_store_dir + "/" + self.formatted_date_str + (".%s" % sv)

            with open(save_loc, 'wb') as pickle_file:
                # cPickle.dump(all, pickle_file)  # TODO is cPickle faster?
                if sv == "model":
                    pkl.dump(obj=self.model, file=pickle_file)
                elif sv == "scaler":
                    pkl.dump(obj=self.scaler, file=pickle_file)

            # model/scaler cleanup (delete the oldest model if there are more than 3)
            sv_ls = utility.get_files_by_type(filepath=self.model_store_dir, filetype=sv)

            count_of_saved: int = len(sv_ls)

            if count_of_saved > keep:
                for i in range(0, count_of_saved - keep):
                    remove_me = self.model_store_dir + "/" + sv_ls[i]
                    os.remove(remove_me)
                    self.logger.info("{0} {1} was successfully deleted.".format(sv, sv_ls[i]))

    def get_performance(self) -> None:
        """ print some performance metrics """

        if self.source == defn.SOURCE_TRAIN_CONST:
            self.predictions = self.model.predict(self.X)
            y_true = self.model_df['class']
            y_pred = self.predictions

            # performance is added at the top of zippylog, hence it can't be found prior to runtime
            self.logger.performance(metrics.classification_report(y_true, y_pred))
            self.logger.performance("balanced accuracy: {0}".format(metrics.balanced_accuracy_score(y_true, y_pred)))

    def get_predictions(self) -> None:

        self.get_X_and_y()

        self.predictions = self.model.predict(self.X)

    def map_predictions_to_files(self) -> None:
        """ update the png filename based on the predictions """

        self.ddo.png_updated_filenames = []

        for file, pred in zip(self.ddo.png_filenames, self.predictions):
            self.logger.debug((file, pred))
            if pred == 0:
                self.ddo.png_updated_filenames.append('cp-' + file)
            elif pred == 1:
                self.ddo.png_updated_filenames.append('ncp-' + file)

        # update the keys of ddo.png_dct
        for og, upd in zip(list(self.ddo.png_dct.keys()), self.ddo.png_updated_filenames):
            self.ddo.png_dct[upd] = self.ddo.png_dct.pop(og)

        self.logger.debug(self.ddo.png_dct.keys())

        # TODO remove this once the in-memory load of pngs are tested and working
        for og, upd in zip(self.ddo.png_filenames, self.ddo.png_updated_filenames):
            source_file = self.ddo.png_dir + "/" + og
            destination_file = self.ddo.png_dir + "/" + upd
            os.rename(src=source_file, dst=destination_file)
