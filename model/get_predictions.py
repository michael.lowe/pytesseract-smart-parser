

def run(pcdo):

    # select most recent model from model directory
    pcdo.get_latest_model()

    # calculate the derived data needed to obtain predictions
    pcdo.calculate_derived_data()

    # apply the model to the derived data
    pcdo.get_predictions()

    # update the filenames with cp- and ncp- based on the predictions (cp-: 0 / ncp-: 1)
    pcdo.map_predictions_to_files()
