import re
import math
import numpy as np
import pandas as pd


class Parser(object):

    def __init__(self, ocr_dataframe):
        self.ocr_dataframe = ocr_dataframe
        self.parsed_dataframe = None
        self.json_data = None
        self.delimiter = "*BREAK"
        self.counter = 0
        self.decimal_pattern = r'\d+'

    def process_work_queue(self, work_queue):
        output = {}
        string_buffer = []
        for i in range(len(work_queue)):
            # logic for item code
            if i == 0 and re.match(self.decimal_pattern, work_queue[i]):
                output['item_number'] = work_queue[i]
            # logic for item group
            elif i == 1 and (re.match (self.decimal_pattern, work_queue[i]) and len(work_queue[i]) == 3):
                output['item_group'] = work_queue[i]
            # logic for quantity
            elif i == len(work_queue) - 3:
                output['quantity'] = work_queue[i]
            # logic for unit price
            elif i == len(work_queue) - 2 and re.match(self.decimal_pattern, work_queue[i]):
                output['unit_price'] = work_queue[i]
            # logic for total price
            elif i == len(work_queue) - 1 and re.match(self.decimal_pattern, work_queue[i]):
                output['total_price'] = work_queue[i]
            # append everything else as the item description
            else:
                string_buffer.append(work_queue[i])

        # update/cleanup quantities
        unit = float(output['unit_price'])
        total = float(output['total_price'])
        output['quantity'] = str(math.trunc(total/unit))
        output['description'] = ' '.join(string_buffer)

        return output

    @staticmethod
    def generate_date(date, length):
        arr = []
        for i in range(length):
            arr.append(date)
        return arr

    def parse_document(self):
        self.parsed_dataframe = self.parse_tampa(self.ocr_dataframe)
        return
 
    def _average(self, data):
        return np.average(data)

    def parse_tampa(self,ocr_df):
        date = []
        item_code = []
        item_group = []
        description = []
        quantity = []
        unit_price = []
        total_price = []
        
        conf = []
        
        lead_line = list(ocr_df[ocr_df['line_num'] == 1]['text'])
        date_single = lead_line[len(lead_line)-1]
        date_conf_line = list(ocr_df[ocr_df['line_num'] ==1 ]['conf'])
        date_conf_item = date_conf_line[len(date_conf_line)-1]
        
        for i in range(1,max(ocr_df['line_num'])):
            line_item = list(ocr_df[ocr_df['line_num'] == i+1]['text'])
            confidence = list(ocr_df[ocr_df['line_num'] == i+1]['conf'])
            struct_size = len(line_item)-1
            date.append(date_single)
            quantity.append(line_item[struct_size-2])
            unit_price.append(line_item[struct_size-1])
            total_price.append(line_item[struct_size])
            item_code.append(line_item[1])
            if(len(line_item) == 5 and line_item[2][1:].isdigit()):
                item_group.append(line_item[2])
                description.append(' '.join(line_item[3:struct_size-2]))
            else:
                item_group.append(line_item[2])
                description.append(' '.join(line_item[3:struct_size-2]))
            confidence = list(ocr_df[ocr_df['line_num'] == i+1]['conf'][1:])
        
            # print(confidence[1:])
            inner_json = {
                "itemCode": confidence[0],
                "itemGroup": confidence[1],
                "description": confidence[2],
                "quantity": confidence[3],
                "unitCost": confidence[4],
                "totalCost": confidence[5]
            }
            conf.append(inner_json)

        
                
        return pd.DataFrame({
            'date': date,
            'itemCode': item_code,
            'itemGroup': item_group,
            'description': description,
            'quantity': quantity,
            'unitPrice': unit_price,
            'totalPrice': total_price,
            'confidence': conf
        })