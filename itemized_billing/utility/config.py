import json

from itemized_billing.utility import definitions as defn


class Config(object):

    def __init__(self):

        document_config_location = defn.BASE_DIR + "/" + "resources/config/document_config.json"

        def parse_json_config(target_dir):
            with open(target_dir) as f:
                data = f.read()
                json.loads(data)
            return data

        self.document_config = json.loads(parse_json_config(document_config_location))


if __name__ == "__main__":
    c = Config()
    print(c.document_config)
