import os

_CURR_DIR = os.path.dirname(os.path.realpath(__file__))

CONVERT_DENSITY = 400  # used when the pdf is converted to png
OCR_OUTPUT_TYPE = 'tsv'
SAVE_SIGNAL = False
SHOW_GRAPHS = False  # set to false when running on server
VERBOSE = True
VIEW_TRANSFORMS = True  # set to false when running on server

# DIRECTORIES
BASE_DIR = "/".join(_CURR_DIR.split("/")[:-2])
MODEL_STORE_DIR = "model"
GET_DOCUMENT_DIR = "resources/test_docs"
OUT_DOCUMENT_DIR = "resources/test_docs/processed_docs"
TRAINING_DOCUMENT_DIR = "resources/training_data"

# MODELING CONSTANTS
SOURCE_TRAIN_CONST = "train"
SOURCE_PREDICT_CONST = "predict"

# ZIPPYLOG
APP_NAME = "ocr-smart-parser"
CONSOLE_LEVEL = "DEBUG"

LOG_CONFIG_DIR = "resources/config"
CONFIG_FILENAME = "logger_config.json"

LOG_DIR = "/tmp/{0}".format(APP_NAME)
