from operator import itemgetter
import logging

import numpy as np
import pandas as pd

from itemized_billing.utility import definitions as defn
from itemized_billing.preprocessing import splice_utility


class AverageInterpolation(object):
    
    def __init__(self, image, rolling_window):
        self.logger = logging.getLogger(__name__)
        self.image = image
        self.flattened_signal = np.array(self.image.convert(mode='L'))  # 'L' for 8-bit black and white
        self.height, self.width = self.flattened_signal.shape
        self.vertical_axis_mean = None
        self.output_image = None
        self.rolling_window = rolling_window  # default

    def calculate_bound(self, data, boundary_type):
        if boundary_type.lower() == 'min':
            asymp = data.describe()['min']
            bool_val = data == asymp
        elif boundary_type.lower() == 'max':
            asymp = data.describe()['max']
            bool_val = data == asymp        
        else:
            _exit_str = "Error encountered: boundary type not specified."
            self.logger.info(_exit_str)
            raise KeyError(_exit_str)

        target_val = data[bool_val].index.to_list()
        self.logger.debug(target_val)
        last_idx = len(target_val)-1

        return target_val[last_idx]

    def fit(self, image_family):
        # TODO @Mike recall the great bug fixing escapade of 4/2. more error checking/exceptions are needed
        self.vertical_axis_mean = self.flattened_signal.mean(axis=1)
        rolling = pd.Series(self.vertical_axis_mean)\
            .rolling(self.rolling_window)\
            .mean()\
            .fillna(np.average(self.vertical_axis_mean)-.25*np.std(self.vertical_axis_mean))

        if image_family == 'bound':
            upper_bound = self.calculate_bound(rolling, 'min')
            lower_bound = self.calculate_bound(rolling, 'max')
            self.logger.debug("lower bound: %s" % lower_bound)
            self.logger.debug("upper bound: %s" % upper_bound)
            self.output_image = self.image.crop((0, lower_bound, self.width, upper_bound))
        elif image_family == 'continuous':
            lower_bound = self.calculate_bound(rolling, 'max')
            self.output_image = self.image.crop((0, lower_bound, self.width, self.height))
        else:
            _exit_str = "No image_family was provided to AverageInterpolation."
            self.logger.info(_exit_str)
            raise ValueError(_exit_str)

        if self.output_image:
            return 1, self.output_image
        else:
            # block for when AverageInterpolation fails
            self.logger.warning("AverageInterpolation failed.")
            return 0, None


class VerticalWhitespace(object):
    """
    algorithm that links whitespace together and saves each piece of the image to its own file
    :return: None
    """

    def __init__(self, image, percentile, use_num_groups, num_groups, cutoff_value, target_idx):
        self.logger = logging.getLogger(__name__)
        self.image = image
        self.flattened_signal = np.array(self.image.convert(mode='L'))
        self.height, self.width = self.flattened_signal.shape
        self.output_image = None

        # model specific parameters
        self.percentile = percentile
        self.use_num_groups = use_num_groups
        self.num_groups = num_groups
        self.cutoff_value = cutoff_value
        self.target_idx = target_idx

        self.histogram_data = []
        self.image_df = pd.DataFrame()
        self.image_array = None

    # link whitespace together
    def fit(self):
        # calculate the average pixel value BY row
        self.histogram_data = [sum(self.flattened_signal[i])/self.width for i in range(self.height)]
        self.image_df['average_row_value'] = self.histogram_data
        pixel_array = np.array(self.image_df['average_row_value']).reshape(-1, 1)
        self.image_df['normalized_average_row_value'] = (pixel_array - min(pixel_array)) / (max(pixel_array) - min(pixel_array))

        np.percentile(self.image_df['average_row_value'], 50)
        percentile = np.quantile(self.image_df['average_row_value'], self.percentile)

        self.image_df.loc[self.image_df.average_row_value >= percentile, 'group'] = "whitespace"
        self.image_df.loc[self.image_df.average_row_value < percentile, 'group'] = "text"

        # here is the sauce that links the whitespace together by index
        whitespace_idx_ranges = []
        counter = 0
        last_seen_whitespace = 0
        last_seen_text = 0
    
        # we traverse 'group' and output list of lists (whitespace_idx_ranges) with the list having two values:
        # [[val1, val2], [val3, val4], ...] constraints val1 < val2, val3 < val4, and so on.
        # each of the inner lists represents a row cutoff that is eventually used to split the image
        for i, val in enumerate(self.image_df['group']):
            if val == 'whitespace' and i != len(self.image_df['group']) - 1:  # avoid out of bounds error
                last_seen_whitespace = i
                if counter == 0:
                    start_idx = i
                counter += 1
            elif val == 'text':
                last_seen_text = i
                if self.image_df['group'][i - 1] == 'whitespace':
                    whitespace_idx_ranges.append([start_idx, i - 1])
                counter = 0
            elif i == len(self.image_df['group']) - 1:  # address out of bounds error here
                if val == 'whitespace':
                    last_seen_whitespace = i
                if last_seen_whitespace == i:
                    whitespace_idx_ranges.append([last_seen_text + 1, last_seen_whitespace])

        # append difference of the indices calculated above (the bigger the difference, the bigger the whitespace)
        for val in whitespace_idx_ranges:
            val.append(np.subtract(val[1], val[0]))
    
        # currently, user can specify how many groups they want
        # OR it can be determined by 'vertical_whitespace_diff' in FILE_CONFIGS
        if self.use_num_groups is False:
            large_whitespace = [val for val in whitespace_idx_ranges
                                if val[2] >= self.cutoff_value]
            large_whitespace.append([self.height, None])
        else:
            large_whitespace = sorted(whitespace_idx_ranges, key=itemgetter(2), reverse=True)[0:self.num_groups]
            large_whitespace = sorted(large_whitespace, key=itemgetter(0))
            large_whitespace.append([self.height, None])

        final_idxs = []
        for i, val in enumerate(large_whitespace):
            if i + 1 < len(large_whitespace):
                final_idxs.append([val[1], large_whitespace[i+1][0]])

        final_idxs = list(map(splice_utility.alter_endpoints_by_delta, final_idxs))
        final_idxs[-1][1] = len(self.image_df)

        if len(final_idxs) > 0:
            final_idx = final_idxs[self.target_idx]
            self.output_image = self.image.crop((0, final_idx[0], self.width, final_idx[1]))

            if defn.SHOW_GRAPHS is True:
                self.output_image.show()

            return 1, self.output_image
        else:
            # block for when VerticalWhitespace fails
            self.logger.warning("VerticalWhitespace failed.")
            return 0, None


class HighFrequency(object):
    """
        algorithm that links whitespace together and saves each piece of the image to its own file
        :return: None
        """
    """
    def __init__(self, image):
        self.image = image
        self.flattened_signal = np.array(self.image.convert(mode='L'))
        self.output_image = None

        self.df = None
        self.image_array = None

    if self.cutoff_type in ["vertical_whitespace", "high_frequency"]:
        # link whitespace together
        # np.percentile(df['average_row_value'], 50)
        percentile = np.quantile(df['average_row_value'],
                                 self.ddo.document_config['percentile'])

        df.loc[df.average_row_value >= percentile, 'group'] = "whitespace"
        df.loc[df.average_row_value < percentile, 'group'] = "text"

        # here is the sauce that links the whitespace together by index
        whitespace_idx_ranges = []
        counter = 0
        last_seen_whitespace = 0
        last_seen_text = 0

        # we traverse 'group' and output list of lists (whitespace_idx_ranges) with the list having two values:
        # [[val1, val2], [val3, val4], ...] constraints val1 < val2, val3 < val4, and so on.
        # each of the inner lists represents a row cutoff that is eventually used to split the image
        for i, val in enumerate(df['group']):
            if val == 'whitespace' and i != len(df['group']) - 1:  # avoid out of bounds error
                last_seen_whitespace = i
                if counter == 0:
                    start_idx = i
                counter += 1
            elif val == 'text':
                last_seen_text = i
                if df['group'][i - 1] == 'whitespace':
                    whitespace_idx_ranges.append([start_idx, i - 1])
                counter = 0
            elif i == len(df['group']) - 1:  # address out of bounds error here
                if val == 'whitespace':
                    last_seen_whitespace = i
                if last_seen_whitespace == i:
                    whitespace_idx_ranges.append([last_seen_text + 1, last_seen_whitespace])

        # append difference of the indices calculated above (the bigger the difference, the bigger the whitespace)
        for val in whitespace_idx_ranges:
            val.append(np.subtract(val[1], val[0]))
            
        elif self.cutoff_type == "high_frequency":
            # histogram of whitespace differences
            fq_histo = np.histogram([val[2] for val in whitespace_idx_ranges], bins='auto')
            # choose bins that have a count greater than config.CUTOFF
            fq_histo_cut_idxs = np.concatenate(np.where(fq_histo[0] > self.ddo.document_config['high_frequency_cutoff']))

            # going 1 before and two after the range to "catch" ones that we might need
            if min(fq_histo_cut_idxs) != 1:  # TODO handle max later... (could run into bounds error)
                fq_histo_cut_idxs = np.append(fq_histo_cut_idxs, [fq_histo_cut_idxs[0] - 1, fq_histo_cut_idxs[-1] + 1, fq_histo_cut_idxs[-1] + 2])

            #candidate_counts = fq_histo[0][slice(fq_histo_cut_idxs[0]-1, fq_histo_cut_idxs[-1]+2, 1)]
            candidate_idxs = [val for val in whitespace_idx_ranges if val[2] in fq_histo_cut_idxs]
            candidate_idxs.append([len(df), len(df), None])
            #print(candidate_idxs)
            #print("-----"*30)
            final_idxs = []
            curr_ls = []
            for i, cand in enumerate(candidate_idxs):
                current = cand[1]

                if i + 1 < len(candidate_idxs):
                    future = candidate_idxs[i+1][1]
                    diff = future - current
                    curr_ls.append(current)
                    #print('curr_ls', curr_ls)

                if diff >= self.ddo.document_config['high_frequency_diff']:
                    #print(current, future)
                    if future != len(df):
                        curr_ls.append(future)
                        final_idxs.append([curr_ls[0], curr_ls[-1]])
                    elif future == len(df):
                        final_idxs.append([final_idxs[-1][1], current])

                    curr_ls = []

            final_idxs = list(map(self.alter_endpoints_by_delta, final_idxs))
            final_idxs[-1][1] = len(df)


    if len(final_idxs) > 0:
        # save the image partitions to disk
        groupings = [image_array[range(ls[0], ls[1])] for ls in final_idxs]

        for i, group in enumerate(groupings):
            img = Image.fromarray(group, 'RGB')

            tmp_filename = self.generate_filepath(defn.BASE_DIR + "/" + self.ddo.png_dir,
                                                  self.ddo.document_id ,
                                                  self.ddo.document_config['cutoff_type'],
                                                  "png",
                                                  "_%s" % (str(i)))
            img.save(tmp_filename)

            if defn.SHOW_GRAPHS:
                img.show()
    else:
        print("No final_idxs were provided.")
    """


# THE CODE BELOW THIS IS A REMNANT INCASE WE WANT TO RE-INVESTIGATE THESE SMOOTHING ALGORITHMS LATER
class Savgol(object):
    """
    y = df['normalized_average_row_value']
    x = np.linspace(0, len(y), len(y))[:, np.newaxis]

    y_hat = signal.savgol_filter(y,
                                 window_length=self.ddo.document_config['savgol_window_size'],
                                 polyorder=self.ddo.document_config['savgol_polynomial_order'])
    minis = signal.argrelextrema(y_hat, np.greater)
    maxis = signal.argrelextrema(y_hat, np.less)

    if defn.SHOW_GRAPHS is True:
        pyplot.plot(x, y)
        pyplot.plot(x, y_hat, color='red')
        pyplot.show()
    """


class Polynomial(object):
    """
    elif self.cutoff_type in ['polynomial', 'poly']:
        print("POLYNOMIAL IS NOT CURRENTLY CONFIGURED.")

        # TODO this whole thing is merely a test, and will not work on a real document atm
        # try regular curve fitting....
        degree = 75
        y = df['normalized_average_row_value']
        x = np.linspace(0, len(y), len(y))[:, np.newaxis]

        x = x.ravel()
        y = y.ravel()

        # calculate polynomial
        z = np.polyfit(x, y, deg=degree)
        f = np.poly1d(z)

        # calculate new x's and y's
        x_new = np.linspace(x[0], x[-1], 50)
        y_new = f(x_new)

        pyplot.plot(x, y, 'o', x_new, y_new)
        pyplot.xlim([x[0] - 1, x[-1] + 1])
        pyplot.show()

        exit()
    """


class KernelDensityEstimator(object):
    """
    elif self.cutoff_type in ["kernel_density_estimator", "KDE", "kde"]:
        print("KDE IS NOT CURRENTLY CONFIGURED.")

        # TODO this whole thing is merely a test, and will not work on a real document atm
        bandwidth = 4
        N = 1000
        np.random.seed(1)

        # scale the data between 0 and 1
        pixel_array = np.array(df['average_row_value']).reshape(-1, 1)
        normalized = (pixel_array - min(pixel_array)) / (max(pixel_array) - min(pixel_array))

        y = np.concatenate((np.random.normal(200, 25, int(0.3 * N)),
                            np.random.normal(600, 50, int(0.7 * N))))[:, np.newaxis]

        x = np.linspace(0, len(y), 1000)[:, np.newaxis]

        true_dens = (0.3 * stats.norm(200, 25).pdf(x[:, 0]) + 0.7 * stats.norm(600, 50).pdf(x[:, 0]))

        fig, ax = pyplot.plot.subplots()
        ax.fill(x[:, 0], true_dens, fc='black', alpha=0.2, label='input distribution')

        kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(y)
        log_dens = kde.score_samples(x)
        ax.plot(x[:, 0], np.exp(log_dens), '-', label="kernel = '{0}'".format('gaussian'))

        ax.text(6, 0.38, "N={0} points".format(N))

        ax.legend(loc='upper left')
        ax.plot(y[:, 0], -0.005 - 0.01 * np.random.random(y.shape[0]), '+k')

        ax.set_xlim(0, 1000)
        ax.set_ylim(0, 0.01)
        pyplot.plot.show()

        exit()
    """
