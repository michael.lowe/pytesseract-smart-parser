import abc
import io
import logging

from PIL import Image

from itemized_billing.utility import definitions as defn
from itemized_billing.preprocessing import splice_cutoffs


class AbstractSplice(abc.ABC):    

    def __init__(self):
        super().__init__()

    @classmethod
    def splice(cls):
        raise NotImplementedError


class Splice(AbstractSplice):

    def __init__(self, ddo):
        AbstractSplice.__init__(self)

        self.logger = logging.getLogger(__name__)

        self.ddo = ddo
        self.document_tag = self.ddo.document_tag
        self.pngs = self.ddo.png_updated_filenames
        
    def splice(self):
        def implementation():
            # done by ddo.image_pointer_or_whatever...
            proceed_status = 1

            for algo in self.ddo.document_config['configured_for']:
                output = {}
                self.logger.info('Using Algorithm: {0}'.format(algo))

                for key, value in self.ddo.png_dct.items():
                    self.logger.info('Applying to: {0}'.format(key))
                    img = Image.open(io.BytesIO(value))

                    if algo == "AverageInterpolation":
                        target_image = splice_cutoffs.AverageInterpolation(image=img,
                                                                           rolling_window=self.ddo.document_config['average_interpolation_rolling_window'])
                        proceed_status, output['cropped-%s' % key] = target_image.fit(image_family="bound")
                        self.logger.debug(output.keys())

                        if defn.SHOW_GRAPHS:
                            output['cropped-%s' % key].show()

                    elif algo == "VerticalWhitespace":
                        target_image = splice_cutoffs.VerticalWhitespace(image=img,
                                                                         percentile=self.ddo.document_config['vertical_whitespace_percentile'],
                                                                         use_num_groups=self.ddo.document_config['vertical_whitespace_use_num_groups'],
                                                                         num_groups=self.ddo.document_config['vertical_whitespace_num_groups'],
                                                                         cutoff_value=self.ddo.document_config['vertical_whitespace_cutoff_value'],
                                                                         target_idx=self.ddo.document_config['vertical_whitespace_target_idx'])

                        proceed_status, output['cropped-%s' % key] = target_image.fit()
                        self.logger.debug(output.keys())

                # control for whether to move onto next algorithm or not
                if proceed_status == 1:
                    return output
                else:
                    continue

        return implementation()
