import numpy as np
import matplotlib.pyplot as plt
from PIL import Image as im
from scipy.ndimage import interpolation as inter


class ImageObject:

    def __init__(self, filepath):
        self.filepath = filepath
        self.skewed_image = im.open(filepath)
        self.corrected_image = None
        self.binary_image = None
        self.width, self.height = self.skewed_image.size
        self.endpoint = None
        self._SKEW_MIN = 2
        self._SKEW_MAX = 10

    def convert_to_binary(self):
        pix = np.array(self.skewed_image.convert('1').getdata(), np.uint8)
        self.binary_image = 1 - (pix.reshape((self.height, self.width)) / 255.0)
        return self

    def fit_scores(self, limit, delta=1):
        # range of potential angles
        calculated_angles = np.arange(-limit, limit + delta, delta)
        print(calculated_angles)
        # return list scores
        scores = [self.score_skewness(self.binary_image, item)[1] for item in calculated_angles]
        print(scores)
        best_score = max(scores)
        best_angle = calculated_angles[scores.index(best_score)]
        print('Best angle: {}'.format(best_angle))
        data = inter.rotate(self.binary_image, best_angle, reshape=False, order=0)
        self.corrected_image = im.fromarray((255 * data).astype("uint8")).convert("RGB")
        return self

    @staticmethod
    def score_skewness(data, angle):
        # interpolate image to fine best skewness score
        data = inter.rotate(data, angle, reshape=False, order=0)
        hist = np.sum(data, axis=1)
        # return binary_hist, angle_score
        return hist, np.sum((hist[1:] - hist[:-1]) ** 2)

    @staticmethod
    def write_corrected_image(image, output_path):
        image.save(output_path)
        return output_path


obj = ImageObject("../docs/skew.png")
obj.convert_to_binary().fit_scores(obj._SKEW_MAX)
filepath = obj.write_corrected_image(obj.corrected_image,"/tmp/test.png")