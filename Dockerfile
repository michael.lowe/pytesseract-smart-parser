ARG VERSION=3.7.2-stretch

FROM python:$VERSION

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev && \
    apt-get install -y tesseract-ocr && \
    apt-get install -y imagemagick

# We copy just the requirements.txt first to leverage Docker cache
COPY requirements.txt /app/requirements.txt

COPY ./itemized_billing /app/itemized_billing

COPY ./resources /app/resources

COPY ./resources /app/model

COPY ./setup.py /app/setup.py

WORKDIR /app

RUN pip install -r requirements.txt