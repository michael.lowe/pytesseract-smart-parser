import json
import uuid
import inspect

import connexion
from flask import Flask, jsonify, make_response, send_file

from itemized_billing.preprocessing import splice
# import itemized billling packages and utilities

from itemized_billing.utility import config
from itemized_billing.utility import definitions as defn
from itemized_billing.utility import doc_data, utility

# import machine learning models
from model import get_predictions, page_classifier


def response_503(filename,serial_id=None,mime_type=None):
    # initialize response to empty to ensure condition logic is upheld
    response = {}
    # initialize error_message to monitor conditional logic flow
    error_message = ''
    
    if serial_id == None and mime_type == None:
        # handle document not accepted response
        error_message = 'Document was not accepted. Please re-upload.'
    elif serial_id == None and mime_type != None:
        # handle document type not supported response
        error_message = 'Document type not supported. Submit pdf.'
    elif serial_id != None and mime_type != None:
        # handle document failed to process response
        error_message = 'Document failed during OCR/Parser processing'
    else:
        error_message = 'No documented error encountered. Please contact application admin.'
    
    response =  make_response(jsonify({
        'document_filename': filename,
        'serial_id': serial_id,
        'mime_type': mime_type,
        'error_message': error_message
    }), 503)

    return response

def response_200(filename,document_tag,serial_id,ib_data,pg_count):
    return make_response(jsonify({
        'document_filename': filename,
        'serial_id': serial_id,
        'ib': {
            'page_count': pg_count,
            'document_tag': document_tag,
            'data': ib_data
        }
    }), 200)


def post_document(doc_tag):
    # Todo: extend end to end functionality of simple request pipeline

    # user should be able to get and post a document via pipeline
    # no database connection required
    # no external filesystem required
    # no queue required
    # no external API call
    # delete dependency on helper code base completely

    # user steps:
    # 1. import document to server
    # 2. begin processing pipe for document
    # 3. OCR/Parse document
    # 4. Return document responsedocument_tagdocument_tagdocument_tagdocument_tagdocument_tag
    # handle the input of IB document by checking the mimetype and content of pdf
    concrete_document = connexion.request.files['pending_document']
    
    if concrete_document == None:
        # should fail from connexion/flask framework parameteres being set
        # if document upload fails or request data is incomplete
        # return 503 status code
        return response_503(concrete_document)
    
    document_filename = concrete_document.filename
    mime_type = concrete_document.mimetype
    
    valid_mimetype = (
        lambda abstract_file:(
            True if abstract_file.mimetype == 'application/pdf' else False
        )
    )
    
    if not valid_mimetype(concrete_document):
        return response_503(document_filename, serial_id=None, mime_type=mime_type)
    
    document_extension = 'pdf'
    # serial_id applied to pages and directories created by DDO object
    serial_id = "{0}_{1}".format(doc_tag,uuid.uuid1())
    # edge cases have been bypassed
    # build document for processing pipeline
    pg_count = None
    json_data = {}
    try:
        ddo = doc_data.DocumentData()
        ddo.target_file = ()
        ddo.document_tag = doc_tag
        ddo.document_name = document_filename
        ddo.document_extension = document_extension
        ddo.document_config = config.Config().document_config[ddo.document_tag]
        ddo.target_file = concrete_document
        ddo.convert_pdf_to_pngs()
        print(ddo.to_string())
        return response_200(document_filename,
                            doc_tag,
                            serial_id,
                            json_data,
                            pg_count )
    except RuntimeError as err:
        print(err)
        return response_503(document_filename,serial_id,mime_type)
