import json
import logging

from model import get_predictions, page_classifier
from itemized_billing.utility import definitions as defn
from itemized_billing.utility import config, doc_data, utility
from itemized_billing.preprocessing import splice
from itemized_billing.zippylog import zippylog


def main():

    zippylog.run(base_dir=defn.BASE_DIR,
                 config_filename=defn.CONFIG_FILENAME,
                 config_dir=defn.LOG_CONFIG_DIR,
                 log_dir=defn.LOG_DIR,
                 console_level=defn.CONSOLE_LEVEL,
                 env_key='LOG_CFG')

    logger = logging.getLogger(__name__)

    # ddo = document data object
    ddo = doc_data.DocumentData()

    # grab the oldest pdf available
    ddo.get_available_document(filename="tampa_01-flhospital.pdf")

    # build the config
    ddo.document_config = config.Config().document_config[ddo.document_tag]

    # convert the pdf to pngs
    ddo.convert_pdf_to_pngs()

    # run page classifier
    pcdo = page_classifier.PageClassifierData(ddo, defn.SOURCE_PREDICT_CONST)  # pcdo = page classifier data object
    get_predictions.run(pcdo)

    # run splicer
    s = splice.Splice(ddo)
    split_dct = s.splice()

    # run parser to output json
    df = utility.process_ocr_documents(split_dct, ddo)
    json_doc = df.to_json(orient="records")
    docs = json.loads(json_doc)
    for doc in docs:
        logger.debug(doc)

    ddo.cleanup()


if __name__ == "__main__":
    main()
